import React, {useState,useEffect} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import { baseUrl } from '../components/Global';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import TextInputM from '../components/TextInputMultiline';
import Spinner from 'react-native-loading-spinner-overlay';
import {Appbar} from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Feather';

const AddCustomerScreen = ({route,navigation}) => {

  const [email, setEmail] = useState({
    value: '',
    error: '',
  });
    
  const [businessname, setBusinessName] = useState({value: '', error: ''});
  const [contactperson, setContactPerson] = useState({value: '', error: ''});
  const [whatsappnumber, setWhatsappNumber] = useState({value: '', error: ''});
  const [address, setAddress] = useState({value: '', error: ''});
  const [remarks, setRemarks] = useState({value: '', error: ''});

  const [loading, setLoading] = useState(false);

  const [sales_executive, setSalesExecutive] = useState([]);
  const [crm_city, cityAreaList] = useState([]);
  const [crm_state, stateList] = useState([]);
  const [selectedStateId, setSelectedStateId] = useState(-1);
  const [selectedCityId, setSelectedCityId] = useState(-1);
  const [loadingState, setLoadingState] = useState(true);
  const [selectedIdSalesExecutive, setSelectedIdSalesExecutive] = useState('');

  useEffect(() => {
    if(loadingState)
      getStateData();
  });

 
  const saveCustomer = () =>{
      navigation.goBack()
  }


   const getSalesExecutivesData = () => {
      
      var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_sales_executives';
      var bearer = 'Bearer ' +route.params.key;
      fetch(url, {
        method: 'POST',
        headers: {
          authentication: bearer,
        }
      })
        .then((response) => response.json())
        .then((responseJson) => {
          for (let i = 0; i < responseJson.data.sales_executive.length; i++) {
            sales_executive.push({label:responseJson.data.sales_executive[i].username,value:responseJson.data.sales_executive[i].id});
          }

        });

    }



   const getStateData = () => {
      setLoadingState(false)
      setLoading(true)
      var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_state';
      var bearer = 'Bearer ' +route.params.key;
      fetch(url, {
        method: 'POST',
        headers: {
          authentication: bearer,
        },
        // body: formData,
      })
      .then((response) => response.json())
      .then((responseJson) => {

        for (let i = 0; i < responseJson.data.state.length; i++) {
          crm_state.push({label:responseJson.data.state[i].name,value:responseJson.data.state[i].name,id:responseJson.data.state[i].id});
        }

        setLoading(false)
      });


      getSalesExecutivesData()

    }

    const onSelectedState = (item) => {
        setSelectedStateId(item.id);
        cityAreaList([]);
        getCityList(item.id)
    }

    const onSelectedCity = (item) => {
       setSelectedCityId(item.value);
    }

    const onSelectedReference = (item) => {
       setSelectedIdSalesExecutive(item.value);
    }

    const getCityList = (id) => {

    setLoading(true)

    let data = {
      state_id:id,
    };
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));

    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_city_area';
    var bearer = 'Bearer '+route.params.key;
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
      body:formData,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        
        for (let i = 0; i < responseJson.data.city_area.length; i++) {
          crm_city.push({label:responseJson.data.city_area[i].name,value:responseJson.data.city_area[i].id});
        }

        cityAreaList(crm_city)
       
        setLoading(false)

      });
    }

  const startLoading = (isShow) => {
    setLoading(isShow);
  };

  const onSavePressed = () => {
    

    if(selectedIdSalesExecutive==''){
      alert('Please select sales executive.');
      return;
    }else if(businessname.value==''){
      alert('Please enter business name.');
      return;
    }else if(contactperson.value==''){
      alert('Please enter contact person.');
      return;
    }else if(whatsappnumber.value==''){
      alert('Please enter whatsapp number.');
      return;
    }else if(email.value==''){
      alert('Please enter email address.');
      return;
    }else if(address.value==''){
      alert('Please enter address.');
      return;
    }else if(selectedStateId==''){
      alert('Please select state.');
      return;
    }else if(selectedCityId==''){
      alert('Please select city.');
      return;
    }else if(remarks.value==''){
      alert('Please enter remarks.');
      return;
    }
   


    startLoading(true);

    let data = {
       sale_executive_id:selectedIdSalesExecutive,
       name:businessname.value,
       person_name:contactperson.value,
       mobile:whatsappnumber.value,
       email:email.value,
       address:address.value,
       state_id:selectedStateId,
       city_id:selectedCityId,
       remarks:remarks.value
    };
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));
    let bearer = 'Bearer ' + route.params.key;
    fetch('http://dalsaniya.com/'+baseUrl[0]+'/app_api/customer/add_customer', {
      method: 'post', //Request Type
      headers: {
        authentication: bearer,
      },
      body: formData,
    })
      .then((response) => response.json())
      //If response is in json then in success
      .then((responseJson) => {
        startLoading(false);
        if (responseJson.statusCode == 401) {
          alert(responseJson.message);
          
        } else if (responseJson.statusCode == 200) {
          alert("Customer is added successfully")
          navigation.goBack()
        }
        console.log(responseJson);
      })
      //If response is not in json then in error
      .catch((error) => {
        startLoading(false);
        alert(JSON.stringify(error));
        console.error(error);
      });
  };


  




  return (
     
     <View style={{flex: 1,
      width: '100%',
      backgroundColor: '#f4fafe'}}>
         <View style={{width: '100%', marginTop: 0}}>
          <Appbar.Header style={{backgroundColor: 'transparent'}} >
            <Appbar.BackAction color='#2DA3FC' onPress={() => navigation.goBack()} />
            <Appbar.Content
              style={{alignItems: 'center'}}
              color='#2DA3FC'
              title="Add Customer"
              titleStyle={{fontSize:16,fontWeight:'bold'}}
            />
            <Appbar.Action icon="menu" color="transparent"  />
          </Appbar.Header>
        </View>

         <Spinner
          visible={loading}
          color="#085cab"
        />
    <ScrollView
      showsVerticalScrollIndicator={true}
      automaticallyAdjustContentInsets={false}
      keyboardDismissMode="on-drag"
      style={{backgroundColor: 'transparent'}}
      keyboardShouldPersistTaps="handled">

        <View
          style={{ width: '100%'}}>
             
          <DropDownPicker
                placeholder="Reference/Event"
                placeholderStyle={{
                    textAlign: 'left',
                    color: "#2DA3FC"
                }}
                items={sales_executive}
                containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:10}}
                style={{backgroundColor: '#fafafa',borderColor:'#2DA3FC',
                            borderWidth:1}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                arrowColor="#2DA3FC"
                onChangeItem={item => onSelectedReference(item)}
              />
        

          <View style={{marginTop: 0}}>
            <TextInput
              label="Business Name"
              returnKeyType="next"
              value={businessname.value}
              onChangeText={(text) => setBusinessName({value: text, error: ''})}
              autoCapitalize="none"
              
            />
              <TextInput
              
              label="Contact Person"
              returnKeyType="next"
              value={contactperson.value}
              onChangeText={(text) => setContactPerson({value: text, error: ''})}
              autoCapitalize="none"
              
            />
              <TextInput
          
              label="WhatsApp Number"
              returnKeyType="next"
              value={whatsappnumber.value}
              onChangeText={(text) => setWhatsappNumber({value: text, error: ''})}
              
              autoCapitalize="none"
              
            />
            <TextInput
              label="Email"
              returnKeyType="next"
              value={email.value}
              onChangeText={(text) => setEmail({value: text, error: ''})}
              
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
            />

            <TextInputM
             multiline={true}
             numberOfLines={5}
              label="Address"
              returnKeyType="next"
              value={address.value}
              onChangeText={(text) => setAddress({value: text, error: ''})}
              
              autoCapitalize="none"
              
            />

          <DropDownPicker
           placeholder="State"
           placeholderStyle={{
                    textAlign: 'left',
                    color: "#2DA3FC"
                }}
            items={crm_state}
            containerStyle={{height: 50,width:'90%',marginLeft:20,marginTop:20,marginBottom:20}}
            style={{backgroundColor: 'transparent',borderColor:'#2DA3FC',
                        borderWidth:1}}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{backgroundColor: '#fafafa'}}
            arrowColor="#2DA3FC"
            onChangeItem={item => onSelectedState(item)}

          />

           <DropDownPicker
            placeholder="City"
            placeholderStyle={{
                    textAlign: 'left',
                    color: "#2DA3FC"
                }}
            items={crm_city}
            containerStyle={{height: 50,width:'90%',marginLeft:20,marginTop:20,marginBottom:20}}
            style={{backgroundColor: 'transparent',borderColor:'#2DA3FC',
                        borderWidth:1}}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{backgroundColor: '#fafafa'}}
            arrowColor="#2DA3FC"
            onChangeItem={item => onSelectedCity(item)}
          />
            
            <TextInputM
              multiline={true}
              numberOfLines={5}
              label="Remarks"
              returnKeyType="done"
              value={remarks.value}
              onChangeText={(text) => setRemarks({value: text, error: ''})}
              autoCapitalize="none"
              
            />

          </View>
   
      <View style={{zIndex: -1}}>
            <Button mode="contained" onPress={onSavePressed}>
              Save
            </Button>
          </View>
      

        </View>

      

     
      </ScrollView>
    
      </View>
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#000',
  }
});

export default AddCustomerScreen;
