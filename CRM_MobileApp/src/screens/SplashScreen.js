import React, {useState,useRef} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  Animated,
} from 'react-native';
import ErpMorbi from '../assets/ErpMorbi';

const SplashScreen = ({navigation}) => {

 const springValue = useRef(new Animated.Value(0.1)).current;

    Animated.spring(
    springValue,
    {
      toValue: 1,
      friction: 1,
      useNativeDriver: true
    }
  ).start(({ finished }) => {
  /* completion callback */
   navigation.reset({
        index: 0,
        routes: [{name: 'CompanyNameScreen'}],
      });
});


  return (
    <View style={styles.container}>
        <Animated.View
                style={[
                  styles.fadingContainer,
                  {
                    transform: [{scale: springValue}]
                  }
                ]}
              >
                <ErpMorbi />
              </Animated.View>
        </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SplashScreen;
