import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Animated,
} from 'react-native';
import {Text} from 'react-native-paper';
import Background from '../components/Background1';
import ErpMorbi from '../assets/ErpMorbi';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import {nameValidator} from '../helpers/nameValidator';
import { changeBaseUrl, baseUrl } from '../components/Global';


const CompanyNameScreen = ({navigation}) => {
  const [cname, setCName] = useState({
    value: 'smsadmin',
    error: '',
  });
  
  const onSubmitPressed = () => {
    const nameError = nameValidator(cname.value);
    if (nameError) {
      isLoading = false;
      setEmail({...cname, error: nameError});
      return;
    }
    changeBaseUrl(cname.value)
    navigation.push('LoginScreen');
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={true}
      automaticallyAdjustContentInsets={false}
      keyboardDismissMode="on-drag"
      style={{backgroundColor: 'white'}}
      contentContainerStyle={{flexGrow: 1}}
      keyboardShouldPersistTaps="handled">
      <Background>
        
        <ErpMorbi />
        <View
          style={{backgroundColor: 'white', width: '100%',marginTop:50, borderRadius: 20}}>
          <View style={{height: '10%',marginTop:20}}>
            <Text style={styles.label}>Company Name</Text>
          </View>
          <View style={{marginTop: 20}}>
            <TextInput
              label="Company Name"
              returnKeyType="done"
              value={cname.value}
              onChangeText={(text) => setCName({value: text, error: ''})}
              error={!!cname.error}
              errorText={cname.error}
              autoCapitalize="none"
           
            />
       
          </View>
          

          <Button style={{marginTop: 20}} mode="contained" onPress={onSubmitPressed}>
            Submit
          </Button>
        </View>


        
        
      </Background>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#000',
  },
  label: {
    color: '#57a3f5',
    margin: 0,
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default CompanyNameScreen;
