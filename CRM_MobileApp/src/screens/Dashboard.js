import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  AppState,
  TouchableWithoutFeedback
} from 'react-native';
import { baseUrl } from '../components/Global';
import Background from '../components/Background';
import Header from '../components/Header';
import {Card} from 'react-native-elements';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {Appbar} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';

//svgs
import Home from '../assets/Home';
import Employee from '../assets/Employee';
import EmployeeGray from '../assets/EmployeeGray';
import CustomerIcon from '../assets/CustomerIcon';


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      loading: 'true',
      spinner: false,
      jsonData: {},
      token: '',
      isHomeVisible:true,
      isCustomerVisible:true,
    };
  }

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
    
   
    this.readLoginData('@LoginData');
  }

   componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    // if (
    //   this.state.appState.match(/inactive|background/) &&
    //   nextAppState === "active"
    // ) {
    //   alert("App has come to the foreground!");
    // }
    this.setState({ appState: nextAppState });
  };

  readLoginData = async (key) => {
    try {
      const userAge = await AsyncStorage.getItem(key);

      if (userAge !== null) {
        let parsed = JSON.parse(userAge);
        this.setState({token: parsed.data.token});
        this.makeRequest();
      }
    } catch (e) {
      alert('Failed to fetch the data from storage');
    }
  };

  makeRequest = () => {
     this.setState({
      spinner: !this.state.spinner,
    });

    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/dashboard';
    var bearer = 'Bearer ' + this.state.token;
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({jsonData: responseJson.data, loading: false});
        this.setState({
          spinner: false,
        });
      })
      .catch((error) =>
      
        this.setState({
          isLoading: false,
          message: 'Something bad happened ' + error,
        }),
      );
  };

  goToReminderList =() =>{
    this.props.navigation.push('ReminderList',{key:this.state.token});
  }

  onclickHome =()=>{
    
  }
  onClickCustomer=()=>{
      this.props.navigation.push('CustomerList',{key:this.state.token});
  }
  onClickEmployee=()=>{
      this.props.navigation.push('EmployeeList',{key:this.state.token});
  }

  render() {
    const jsonDashboard = this.state.jsonData;

    var POINTS = jsonDashboard.pending_talk;
    var MAX_POINTS = jsonDashboard.total_talk;
    var fill = (POINTS / MAX_POINTS) * 100;


    return (
      <Background>
        
        <View
          style={{
            flex: 1,
            width: '100%',
            height:'100%',
            position: 'relative',
            marginTop: -40,
            backgroundColor:'transparent'
          }}>
          <Spinner
            visible={this.state.spinner}
            color="#085cab"
          />


         {this.state.isHomeVisible && <ScrollView
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            style={styles.scrollViewStyle}>
            <Header></Header>
            <TouchableWithoutFeedback onPress={() => this.goToReminderList()}>
            <View style={styles.module_parent_view} >
            
              <Card containerStyle={styles.module_card}  >
                <View style={{position: 'absolute', marginTop: 30}}>
                  <Text style={styles.module_label_header}> Reminder </Text>
                </View>

                <View style={{position: 'absolute', right: 1}}>
                  <AnimatedCircularProgress
                    size={100}
                    width={8}
                    fill={fill}
                    rotation={0}
                    lineCap="round"
                    tintColor="#09AAFD"
                    onAnimationComplete={() =>
                      console.log('onAnimationComplete')
                    }
                    backgroundColor="#DDDFE0">
                    {(fill) => (
                      <View>
                        <Text
                          style={{
                            textAlign: 'center',
                            color: 'black',
                            fontSize: 18,
                            fontWeight: 'bold',
                          }}>
                          {MAX_POINTS == 0 && POINTS == 0
                            ? 0
                            : POINTS}
                          /{MAX_POINTS}
                        </Text>
                      </View>
                    )}
                  </AnimatedCircularProgress>
                </View>
              </Card>
              
            </View>
            </TouchableWithoutFeedback>

            <View style={styles.module_parent_view}>
              <Card containerStyle={styles.module_card}>
                <View style={{position: 'absolute'}}>
                  <Text style={styles.module_label_header}> Orders </Text>
                </View>

                <View
                  style={{
                    position: 'absolute',
                    flex: 1,
                    left: 1,
                    marginTop: 70,
                  }}>
                  <Text style={styles.rightTopText}>
                    {jsonDashboard.order_count}
                  </Text>
                  <Text style={styles.rightBottomText}>Count</Text>
                </View>
                <View style={{position: 'absolute', right: 1, marginTop: 70}}>
                  <Text style={styles.rightTopText}>
                    {jsonDashboard.order_total}
                  </Text>
                  <Text style={styles.rightBottomText}>Total</Text>
                </View>
              </Card>
            </View>

            <View style={styles.module_parent_view}>
              <Card containerStyle={styles.module_card}>
                <View style={{position: 'absolute'}}>
                  <Text style={styles.module_label_header}> Less Stock </Text>
                </View>

                <View
                  style={{
                    position: 'absolute',
                    flex: 1,
                    left: 1,
                    marginTop: 70,
                  }}>
                  <Text style={styles.rightTopText}>
                    {jsonDashboard.less_stock_count}
                  </Text>
                  <Text style={styles.rightBottomText}>Count</Text>
                </View>
                <View style={{position: 'absolute', right: 1, marginTop: 70}}>
                  <Text style={styles.rightTopText}>
                    {jsonDashboard.less_stock_total}
                  </Text>
                  <Text style={styles.rightBottomText}>Total</Text>
                </View>
              </Card>
            </View>

            <View style={styles.module_parent_view}>
              <Card containerStyle={styles.module_card}>
                <View style={{position: 'absolute'}}>
                  <Text style={styles.module_label_header}> Add Stock </Text>
                </View>

                <View
                  style={{
                    position: 'absolute',
                    flex: 1,
                    left: 1,
                    marginTop: 70,
                  }}>
                  <Text style={styles.rightTopText}>
                    {jsonDashboard.stock_count}
                  </Text>
                  <Text style={styles.rightBottomText}>Count</Text>
                </View>
                <View style={{position: 'absolute', right: 1, marginTop: 70}}>
                  <Text style={styles.rightTopText}>
                    {jsonDashboard.stock_total}
                  </Text>
                  <Text style={styles.rightBottomText}>Total</Text>
                </View>
              </Card>
            </View>
            <View style={{height:150}}>
            </View>
          </ScrollView>}

          <Card  containerStyle={styles.module_card2}>
              <View style={{flexDirection:'row'}}>
                
                <TouchableOpacity style={{marginTop:5}} onPress={this.onclickHome}>
                    <Home />
                  </TouchableOpacity>

                 <TouchableOpacity style={{marginTop:5,marginLeft:40}} onPress={this.onClickCustomer}>
                        <CustomerIcon
                          />
                  </TouchableOpacity>

                <TouchableOpacity style={{marginTop:5,marginLeft:40}} onPress={this.onClickEmployee}>
                      <EmployeeGray
                      />
                  </TouchableOpacity>
              </View>
          </Card>

        </View>


      </Background>
    );
  }
}


const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#000',
  },
  module_parent_view: {
    width: '100%',
  },
  module_label_header: {
    fontWeight: 'bold',
    fontSize: 20,
    justifyContent: 'center',
    color: '#2DA3FC',
  },
  module_card: {
    height: 130,
    backgroundColor: 'white',
    borderRadius: 8,
    borderColor: '#E0F1FE',
    shadowOpacity: 0.25,
    shadowRadius: 3,
    elevation: 5,
    marginBottom: 10,
  },
  module_card2: {
    height: 70,
    width:250,
    position:'absolute',
    backgroundColor: 'white',
    borderRadius: 35,
    borderColor: '#57a3f5',
    borderWidth:1,
    elevation: 5,
    alignItems:'center',
    alignSelf:'center',
    bottom:20
  },
  rightTopText: {
    color: '#2DA3FC',
    fontWeight: 'bold',
    fontSize: 16,
    includeFontPadding: false,
    paddingTop: 4,
    paddingLeft: 4,
  },
  rightBottomText: {
    color: '#2DA3FC',
    fontSize: 10,
    includeFontPadding: false,
    paddingBottom: 4,
    paddingLeft: 4,
  },
  scrollViewStyle: {
    width: '100%',
    marginBottom:-50
  },
});

export default Dashboard;
