import React, {useState,useEffect} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  Image,
} from 'react-native';
import { baseUrl } from '../components/Global';
import {Text} from 'react-native-paper';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import TextInputM from '../components/TextInputMultiline';
import Spinner from 'react-native-loading-spinner-overlay';
import {Appbar} from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import {Calendar} from 'react-native-calendars';
import moment from 'moment';

const AddTalkScreen = ({route,navigation}) => {
  
   

  const [loading, setLoading] = useState(false);
  const [loadingTalk, setLoadingTalk] = useState(true);

  const [sales_executive, setSalesExecutive] = useState([]);
  const [crm_status, setCrmStatus] = useState([]);
  const [crm_issue, setCrmIssue] = useState([]);
  const [crm_conversation_mode, setCrmConversationMode] = useState([]);

  const [crm_response, setCrmResponse] = useState([]);
  const [doc_send, setCrmSend] = useState([]);

  const [selected, setSelected] = useState('');
  const [vCalendar, setVisibleCalendar] = useState(false);
  const [startDate, setStartDate] = useState('');
  const [reminderDate, setReminderDate] = useState('');
  const [isStartDate, setIsStartDate] = useState(true);

  const currentDate = moment(new Date()).format('YYYY-MM-DD')



  const [selectedIdIssue, setSelectedIdIssue] = useState('');
  const [selectedIdConversionMode, setSelectedIdConversionMode] = useState('');
  const [selectedIdSalesExecutive, setSelectedIdSalesExecutive] = useState('');
  const [selectedIdCrmResponse, setSelectedIdCrmResponse] = useState('');
  const [selectedIdCrmStatus, setSelectedIdCrmStatus] = useState('');
  const [selectedIdDocSend, setSelectedIdDocSend] = useState('');
  const [resnarration, setResponseNarration] = useState({
    value: '',
    error: '',
  });


  useEffect(() => {
      if(loadingTalk)
      getTalkFormData();
  });


  const startLoading = (isShow) => {
    setLoading(isShow);
  };

  const getTalkFormData = () => {
    startLoading(true);
    setLoadingTalk(false)
    let bearer = 'Bearer ' + route.params.key;
    fetch('http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_crm_form_data', {
      method: 'post', //Request Type
       headers: {
        authentication: bearer,
      },
      // body: formData,
    })
      .then((response) => response.json())
      //If response is in json then in success
      .then((responseJson) => {

         for (let i = 0; i < responseJson.data.sales_executive.length; i++) {
          sales_executive.push({label:responseJson.data.sales_executive[i].username,value:responseJson.data.sales_executive[i].id});
        }


         for (let i = 0; i < responseJson.data.crm_status.length; i++) {
          crm_status.push({label:responseJson.data.crm_status[i].name,value:responseJson.data.crm_status[i].id});
        }

        

         for (let i = 0; i < responseJson.data.crm_issue.length; i++) {
          crm_issue.push({label:responseJson.data.crm_issue[i].name,value:responseJson.data.crm_issue[i].id});
        }


         for (let i = 0; i < responseJson.data.crm_response.length; i++) {
          crm_response.push({label:responseJson.data.crm_response[i].name,value:responseJson.data.crm_response[i].id});
        }


         for (let i = 0; i < responseJson.data.doc_send.length; i++) {
          doc_send.push({label:responseJson.data.doc_send[i].name,value:responseJson.data.doc_send[i].id});
        }

        startLoading(false);

      })
      //If response is not in json then in error
      .catch((error) => {
        startLoading(false);
        console.error(error);
      });

  }


  const getConversationModeData = (iId) => {
    console.log("Issue id is..."+iId)
    startLoading(true);
    let data = {issued_id:iId};
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));
    let bearer = 'Bearer '+route.params.key;
    fetch('http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_conversion_mode',{
      method: 'post', //Request Type
       headers: {
        authentication: bearer,
      },
      body: formData,
    })
      .then((response) => response.json())
      //If response is in json then in success
      .then((responseJson) => {

            startLoading(false);
          
            console.log(JSON.stringify(responseJson.data.conversation_mode))
            // setCrmConversationMode([])
            for (let i = 0; i < responseJson.data.conversation_mode.length; i++) {
                crm_conversation_mode.push({label:responseJson.data.conversation_mode[i].name,value:responseJson.data.conversation_mode[i].id});
            }     
            console.log(crm_conversation_mode.length)

      })
      //If response is not in json then in error
      .catch((error) => {
        startLoading(false);
        console.error(error);
      });

  }



  const onSavePressed = () => {
    
      if (startDate=='') {
          alert('Please select start date.')
          return;
      }else if(selectedIdIssue==''){
          alert('Please select issue.')
          return;
      }else if(selectedIdConversionMode==''){
          alert('Please select Conversation Mode.')
          return;
      }else if(selectedIdSalesExecutive==''){
          alert('Please select sales executive.')
          return;
      }else if(selectedIdCrmResponse == ''){
          alert('Please select response.')
          return;
      }else if(resnarration.value==''){
          alert('Please enter response narration.')
          return;
      }else if(reminderDate==''){
          alert('Please select reminder date.')
          return;
      }else if(selectedIdCrmStatus==''){
          alert('Please select status.')
          return;
      }else if(selectedIdDocSend==''){
          alert('Please select doc send.')
          return;
      }


    startLoading(true);

    let data = {
      customer_id:route.params.cID,
      entry_date:startDate,
      remaind_date:reminderDate,
      conversation_status_id:selectedIdCrmStatus,
      issued_id:selectedIdIssue,
      conversion_mode_id:selectedIdConversionMode,
      response_id:selectedIdCrmResponse,
      talk_narration:resnarration.value,
      doc_send_by_id:selectedIdDocSend,
      display_also_id:[selectedIdSalesExecutive],
      called_by_id:selectedIdIssue
    };
    
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));
    var bearer = 'Bearer ' +route.params.key;
    fetch('http://dalsaniya.com/'+baseUrl[0]+'/app_api/crm_conversation/add_talk', {
      method: 'post', //Request Type
      headers: {
        authentication: bearer,
      },
      body: formData,
    })
      .then((response) => response.json())
      //If response is in json then in success
      .then((responseJson) => {

        startLoading(false);
        if (responseJson.statusCode == 401) {
          alert(responseJson.message);
          
        } else if (responseJson.statusCode == 200) {
          alert("Talk is added successfully")
          navigation.goBack()
        }
        console.log(responseJson);
      })
      //If response is not in json then in error
      .catch((error) => {
        startLoading(false);
        alert(JSON.stringify(error));
        console.error(error);
      });
  };

   const onDayPress = day => {
    if(isStartDate)
      setStartDate(day.dateString)
    else
      setReminderDate(day.dateString)

    setVisibleCalendar(false)
    setIsStartDate(true)
  };

const onDatePressed = () =>{
  setVisibleCalendar(true)
  setIsStartDate(true)
}

const onReminderDatePressed = ()=>{
  setVisibleCalendar(true)
  setIsStartDate(false)
}

  


const onSelectedIssue = (item) => {
  setSelectedIdIssue(item.value)
  getConversationModeData(item.value)
}

const onSelectedMarketing = (item) => {
  setSelectedIdSalesExecutive(item.value)
}


const onSelectedResponse = (item) => {
  setSelectedIdCrmResponse(item.value)
}

const onSelectedStatus = (item) => {
  setSelectedIdCrmStatus(item.value)
}

const onSelectedDocSend = (item) => {
    setSelectedIdDocSend(item.value)
}

const onSelectedConversionMode = (item) => {
    setSelectedIdConversionMode(item.value)
}

  return (
     
     <View style={{flex: 1,
          width: '100%',
          backgroundColor: '#f4fafe'}}>

               <View style={{width: '100%', marginTop: 0}}>
                <Appbar.Header style={{backgroundColor: 'transparent'}} >
                  <Appbar.BackAction color='#2DA3FC' onPress={() => navigation.goBack()} />
                  <Appbar.Content
                    style={{alignItems:'center'}}
                    color='#2DA3FC'
                    title="Add Talk"
                    titleStyle={{fontSize:16,fontWeight:'bold'}}
                  />
                  <Appbar.Action icon="menu" color="transparent"  />
                </Appbar.Header>
              </View>

         <Spinner
          //visibility of Overlay Loading Spinner
          visible={loading}
          color="#085cab"
        />

    <ScrollView
      showsVerticalScrollIndicator={true}
      automaticallyAdjustContentInsets={false}
      keyboardDismissMode="on-drag"
      style={{backgroundColor: 'transparent'}}
      keyboardShouldPersistTaps="handled">

        <View
          style={{ width: '100%'}}>

        <TouchableOpacity
              onPress={onDatePressed}>
            <View style={{
                        paddingVertical: 15,
                        paddingHorizontal: 10,
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginLeft:20,
                        marginRight:20,
                        marginTop:20,
                        height:50,
                        borderColor:'#2DA3FC',
                        borderWidth:1
                    }}>

                    {startDate=='' && <Text style={{
                                fontSize: 16,
                                color: "#2DA3FC"
                            }}>Start Date</Text>}

                        <Text style={{
                                fontSize: 16,
                                color: "#2DA3FC"
                            }}>{startDate}</Text>
                      

                            <Icon name="calendar" size={15} color="#2699fb" />

                    </View>
                    </TouchableOpacity>


        <DropDownPicker
              placeholder="Issued"
              placeholderStyle={{
                  textAlign: 'left',
                  color: "#2DA3FC"
              }}
                items={crm_issue}
                containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:20}}
                style={{backgroundColor: '#fafafa',borderColor:'#2DA3FC',
                            borderWidth:1}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                arrowColor="#2DA3FC"
                onChangeItem={item => onSelectedIssue(item)}
              />

          <DropDownPicker
                placeholder="Conversation Mode"
                placeholderStyle={{
                    textAlign: 'left',
                    color: "#2DA3FC"
                }}
                items={crm_conversation_mode}
                containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:20}}
                style={{backgroundColor: '#fafafa',borderColor:'#2DA3FC',
                            borderWidth:1}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                arrowColor="#2DA3FC"
                onChangeItem={item => onSelectedConversionMode(item)}
              />

             
          <DropDownPicker
              placeholder="Marketing"
              placeholderStyle={{
                  textAlign: 'left',
                  color: "#2DA3FC"
              }}
                items={sales_executive}
                containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:10}}
                style={{backgroundColor: 'transparent',borderColor:'#2DA3FC',
                            borderWidth:1}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                arrowColor="#2DA3FC"
                onChangeItem={item => onSelectedMarketing(item)}
              />

             <DropDownPicker
                  placeholder="Normal"
                  placeholderStyle={{
                      textAlign: 'left',
                      color: "#2DA3FC"
                  }}
                    items={crm_response}
                    containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:20}}
                    style={{backgroundColor: '#fafafa',borderColor:'#2DA3FC',
                                borderWidth:1}}
                    itemStyle={{
                      justifyContent: 'flex-start',
                    }}
                    dropDownStyle={{backgroundColor: '#fafafa'}}
                    arrowColor="#2DA3FC"
                    onChangeItem={item => onSelectedResponse(item)}
                  />

          <TextInputM
              multiline={true}
              numberOfLines={5}
              label="Response Narration"
              returnKeyType="done"
              value={resnarration.value}
              onChangeText={(text) => setResponseNarration({value: text, error: ''})}
              autoCapitalize="none"
              
            />

<TouchableOpacity
              onPress={onReminderDatePressed}>

          <View style={{paddingVertical: 15,
                        paddingHorizontal: 10,
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginLeft:20,
                        marginRight:20,
                        height:50,
                        borderColor:'#2DA3FC',
                        borderWidth:1
                    }}>

                    {reminderDate=='' && <Text style={{
                                fontSize: 16,
                                color: "#2DA3FC"
                            }}>Reminder Date</Text>}

                        <Text style={{fontSize: 16,color: "#2DA3FC"}}>{reminderDate}</Text>
                        <Icon name="calendar" size={15} color="#2699fb" />

                    </View>

         </TouchableOpacity>          

           <DropDownPicker
                placeholder="Remind"
                placeholderStyle={{
                    textAlign: 'left',
                    color: "#2DA3FC"
                }}
                items={crm_status}
                containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:10}}
                style={{backgroundColor: '#fafafa',borderColor:'#2DA3FC',
                            borderWidth:1}}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                arrowColor="#2DA3FC"
                onChangeItem={item => onSelectedStatus(item)}
              />

             <DropDownPicker
                  placeholder="Called By"
                   placeholderStyle={{
                      textAlign: 'left',
                      color: "#2DA3FC"
                  }}
                    items={doc_send}
                    containerStyle={{height: 50,width:'90%',marginLeft:18,marginTop:20,marginBottom:20}}
                    style={{backgroundColor: '#fafafa',borderColor:'#2DA3FC',
                                borderWidth:1}}
                    itemStyle={{
                      justifyContent: 'flex-start',
                    }}
                    dropDownStyle={{backgroundColor: '#fafafa'}}
                    arrowColor="#2DA3FC"
                    onChangeItem={item => onSelectedDocSend(item)}
                  />
        
     
   
          <View style={{zIndex: -1}}>
            <Button mode="contained" onPress={onSavePressed}>
              Save
            </Button>
          </View>

        </View>

      </ScrollView>






         { vCalendar && <View style={{position:'absolute',backgroundColor:'transparent',flex: 1,
              width: '100%',
              height:'100%',
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',}}>

                <Calendar
                    current={currentDate}
                    minDate={currentDate}
                    style={styles.calendar}
                    onDayPress={onDayPress}
                  
                  />
          </View>
        }
      </View>




  );
};

const styles = StyleSheet.create({
  calendar: {
    marginBottom: 10
  },
  spinnerTextStyle: {
    color: '#000',
  },
});

export default AddTalkScreen;
