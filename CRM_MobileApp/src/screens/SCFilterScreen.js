import React,{Component} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { baseUrl } from '../components/Global';
import {Text} from 'react-native-paper';
import Background from '../components/Background1';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import {theme} from '../core/theme';
import {emailValidator} from '../helpers/emailValidator';
import {passwordValidator} from '../helpers/passwordValidator';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import {BottomNavigation, Menu, Appbar} from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Feather';

class SCFilterScreen extends Component {

constructor(props) {
    super(props);
    this.state = {
      loading:false,
      token:"",
      stateOfCustomer:[],
      cityAreaList:[],
      selectedStateId:"",
      selectedCityId:"",
      dataPass:{},

    }
  }

  componentDidMount() {
    // this.readLoginData('@LoginData');
    this.getFilterData();
  }


  // readLoginData = async (key) => {
  //   try {
  //     const userAge = await AsyncStorage.getItem(key);
  //     if (userAge !== null) {
  //       let parsed = JSON.parse(userAge);
  //       this.state.token=parsed.data.token;
        
  //       this.getFilterData();
  //     }
  //   } catch (e) {
  //     alert('Failed to fetch the data from storage');
  //   }
  // };

    getFilterData = () => {
      this.setState({loading:true})

    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_state';
    var bearer = 'Bearer ' +this.props.route.params.key;
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
      // body: formData,
    })
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({loading:false})

        for (let i = 0; i < responseJson.data.state.length; i++) {
          this.state.stateOfCustomer.push({label:responseJson.data.state[i].name,value:responseJson.data.state[i].name,id:responseJson.data.state[i].id});
        }
      });

    }

  

     onPressState = (item) => {
        this.setState({selectedStateId:item.id,cityAreaList:[]});
        this.getCityList(item.id)
    }

    onSelectedCity = (item) => {
        this.setState({selectedCityId:item.value});
    }

    getCityList = (id) => {

    this.setState({loading:true})

    let data = {
      state_id:id,
    };
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));

    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/common/get_city_area';
    var bearer = 'Bearer ' +this.props.route.params.key;
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
      body:formData,
    })
      .then((response) => response.json())
      .then((responseJson) => {

         for (let i = 0; i < responseJson.data.city_area.length; i++) {
          this.state.cityAreaList.push({label:responseJson.data.city_area[i].name,value:responseJson.data.city_area[i].id});
        }
        this.setState({loading:false})


      });
    }

  onSavePressed = () => {
    this.props.navigation.navigate('EmployeeList',{dataPass:{state:this.state.selectedStateId,city:this.state.selectedCityId}});
  };

render(){


  return (
     
     <View style={{flex: 1,
    width: '100%',
    backgroundColor: '#f4fafe'}}>
         <View style={{width: '100%', marginTop: 0}}>
          <Appbar.Header style={{backgroundColor: 'transparent'}} >
            <Appbar.BackAction color='#2DA3FC' onPress={() => this.props.navigation.goBack()} />
            <Appbar.Content
              style={{alignItems: 'center'}}
              color='#2DA3FC'
              title="Filter"
              titleStyle={{fontSize:16,fontWeight:'bold'}}
            />
            <Appbar.Action icon="menu" color="transparent"  />
          </Appbar.Header>
        </View>

         <Spinner
          //visibility of Overlay Loading Spinner
          visible={this.state.loading}
          color="#2DA3FC"
          //Text style of the Spinner Text
          textStyle={styles.spinnerTextStyle}
        />
    <ScrollView
      showsVerticalScrollIndicator={true}
      automaticallyAdjustContentInsets={false}
      keyboardDismissMode="on-drag"
      style={{backgroundColor: 'transparent'}}
      keyboardShouldPersistTaps="handled">

        <View
          style={{ width: '100%',}}>
            

             <DropDownPicker
            placeholder="State"
            placeholderStyle={{
              textAlign: 'left',
              color: "#2DA3FC",
              fontWeight:'bold'
            }}
            items={this.state.stateOfCustomer}
            containerStyle={{height: 50,width:'90%',marginLeft:20,marginTop:20}}
            style={{backgroundColor: 'transparent',borderColor:'#2DA3FC',
                        borderWidth:1}}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{backgroundColor: '#fafafa'}}
            onChangeItem={item => this.onPressState(item)}
            arrowColor="#2DA3FC"
          />

           <DropDownPicker
            placeholder="City"
            placeholderStyle={{
              textAlign: 'left',
              color: "#2DA3FC",
              fontWeight:'bold'
            }}
            items={this.state.cityAreaList}
            containerStyle={{height: 50,width:'90%',marginLeft:20,marginTop:20,marginBottom:20}}
            style={{backgroundColor: 'transparent',borderColor:'#2DA3FC',
                        borderWidth:1}}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{backgroundColor: '#fafafa'}}
            arrowColor="#2DA3FC"
            onChangeItem={item => this.onSelectedCity(item)}
          />
        </View>
        <View style={{zIndex: -1}}>
            <Button mode="contained"  onPress={this.onSavePressed}>
              Apply
            </Button>
        </View>

      </ScrollView>
    
      </View>
  );
}
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#000',
  },
  forgotPassword: {
    width: '95%',
    alignItems: 'flex-end',
    marginBottom: 24,
    color: 'white',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 15,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  label: {
    color: '#57a3f5',
    margin: 0,
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default SCFilterScreen;
