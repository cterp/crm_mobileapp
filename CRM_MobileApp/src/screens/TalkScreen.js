import React, {Component} from 'react';
import {
  StatusBar,
  StyleSheet,
  View,
  Text,
  ScrollView,
  Alert,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  Linking,
  ToastAndroid,
  ImageBackground,
  ActivityIndicator,
  Image
} from 'react-native';
import { baseUrl } from '../components/Global';
import Background from '../components/Background';
import { Appbar, Searchbar,Button,Badge } from 'react-native-paper';
import { FAB } from 'react-native-paper';

//svgs
import ReminderTime from '../assets/ReminderTime';


class TalkScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      spinner: false,
      talkList: [],
      reminderList:[],
      showDialog: false,
      isTalkSelectd:true
    };

    this.pageIndex=0;
    this.nomoredata=false;

    this.pageIndexReminder=0;
    this.nomoredataReminder=false;
  }
  componentDidMount() {
    this.getAllTalkList(this.pageIndex);
    this.getReminderList(this.pageIndexReminder);
  }

  getAllTalkList = (pageNo) => {

    this.setState({
      spinner: true,
    });
    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/crm_conversation';
    var bearer = 'Bearer ' + this.props.route.params.key;
    let data = {
        start : pageNo,
        limit : 10,
        status:"0",
        customer_id:this.props.route.params.id
    };
  
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
      body: formData,
    })
      .then((response) => response.json())
      .then((responseJson) => {

        console.log("Talk list ---"+JSON.stringify(responseJson))
        
        var arrCustomerList = [];
        var customerListData = responseJson.data.crm_data;

        for (let i = 0; i < customerListData.length; i++) {
          arrCustomerList.push(customerListData[i]);
        }

        this.setState({
          talkList:[...this.state.talkList,...arrCustomerList]
        });

        this.setState({
          spinner: false,
        });

         if(arrCustomerList.length==0||arrCustomerList.length < 10){
            if(pageNo!=0)
            alert('No more data found.')
            this.nomoredata=true;
        } 

      })
      .catch((error) =>
        alert('Something bad happened ' + error),
      );
  };

    getReminderList = (pageNo) => {

    this.setState({
      spinner: true,
    });
    var url = 'http://dalsaniya.com/'+baseUrl[0]+'/app_api/crm_conversation';
    var bearer = 'Bearer ' + this.props.route.params.key;
    let data = {
        start : pageNo,
        limit : 10,
        status:"1",
        customer_id:this.props.route.params.id
    };
  
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));
    fetch(url, {
      method: 'POST',
      headers: {
        authentication: bearer,
      },
      body: formData,
    })
      .then((response) => response.json())
      .then((responseJson) => {

        var arrReminderList = [];
        for (let i = 0; i < responseJson.data.crm_data.length; i++) {
          arrReminderList.push(responseJson.data.crm_data[i]);
        }

        this.setState({
          reminderList:[...this.state.reminderList,...arrReminderList]
        });

        this.setState({
          spinner: false,
        });

         if(arrReminderList.length==0||arrReminderList.length < 10){
            if(pageNo!=0)
            alert('No more data found.')
            this.nomoredataReminder=true;
        } 

      })
      .catch((error) =>
        alert('Something bad happened ' + error),
      );
  };

  loadMoreDataOfTalkList = () => {
    
      if(!this.nomoredata){
        var nextPage = this.pageIndex+10
        this.pageIndex=nextPage;
        
        this.getAllTalkList(nextPage)

      }
  };

  loadMoreDataOfReminderList = () => {
    
      if(!this.nomoredataReminder){
        var nextPage = this.pageIndexReminder+10
        this.pageIndexReminder=nextPage;
        this.getReminderList(nextPage)
      }

  };


  onCancel() {
    console.log("CANCEL")
    this.setState({visible:false});
  }
  onOpen() {
    console.log("OPEN")
    this.setState({visible:true});
  }

  showAllTalkList = () => {
    this.setState({isTalkSelectd:true})
  }

  showReminderList =()=>{
    this.setState({isTalkSelectd:false})
  }

  openAddTalkScreen=()=>{
    this.props.navigation.push('AddTalkScreen',{key:this.props.route.params.key,cID:this.props.route.params.id})
  }


  render() {

  const renderItemAllTalk = ({ item }) => {
     return  <View style={{flex: 1, marginLeft: 40, marginTop:20 }} key={item.response_name}>

                      <Text style={styles.rightTopText} numberOfLines={1} ellipsizeMode='tail'>{item.talk_date}</Text>
                      <Text style={styles.rightBottomText}>{item.response_name}</Text>
                      <Text style={styles.rightBottomText}>{item.talk_narration}</Text>
                    
                      {item.remaind_date!="" && 

                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginLeft:5 }}>
                                <ReminderTime />
                                <Text style={{
                                        fontSize: 14,
                                        color: "#2DA3FC",
                                        marginLeft:5,
                                        fontWeight:'bold',
                                        lineHeight: 20,
                                    }}>{item.remaind_date}</Text>
                                 
                        </View>
                      }
            </View>  

    
  };


  const renderItemReminder = ({ item }) => {

    return <View style={{flex: 1, marginLeft: 40, marginTop:20 }} key={item.talk_date}>

                      <Text style={styles.rightTopText} numberOfLines={1} ellipsizeMode='tail'>{item.talk_date}</Text>
                      <Text style={styles.rightBottomText}>{item.response_name}</Text>
                      <Text style={styles.rightBottomText}>{item.talk_narration}</Text>
                    
                      {item.remaind_date!="" && <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginLeft:5 }}>
                            <ReminderTime
                                  />
                                <Text style={{
                                        fontSize: 14,
                                        color: "#2DA3FC",
                                        marginLeft:5,
                                        fontWeight:'bold',
                                        lineHeight: 20,
                                    }}>{item.remaind_date}</Text>
                                 
                            </View>}
            </View>;
  };

  const FlatListItemSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "100%",
            marginTop:20,
            marginLeft:40,
            backgroundColor: "lightgray",
          }}
        />
      );
}



    return (
      <Background>

    
        <View style={{width: '100%', marginTop: 0}}>
                <Appbar.Header style={{backgroundColor: 'transparent'}} >
                  <Appbar.BackAction color='#2DA3FC' onPress={() => this.props.navigation.goBack()} />
                  <Appbar.Content
                    style={{alignItems:'center'}}
                    color='#2DA3FC'
                    title="Talk"
                    titleStyle={{fontSize:16,fontWeight:'bold'}}
                  />
                  <Appbar.Action icon="menu" color="transparent"  />
                </Appbar.Header>
              </View>

         <View style={styles.container}>

           {this.state.isTalkSelectd && <View style={styles.buttonContainer}>
              
                <Button mode="text" uppercase={false} color="#2DA3FC" labelStyle={{fontWeight:'bold'}} >All talk</Button>
                
              </View>}

              {this.state.isTalkSelectd &&
              <TouchableOpacity onPress={() => this.showReminderList()}><View style={styles.buttonContainer2}>
              
                <Button mode="text" uppercase={false} color="#2DA3FC" >Reminder</Button>
                <Badge style={{backgroundColor:'#C4E4FD',alignSelf:'center',color:"#2DA3FC",fontWeight:'bold'}} >{this.state.reminderList.length}</Badge>
                
              </View></TouchableOpacity>}


          {!this.state.isTalkSelectd && 

              <View style={styles.buttonContainer2}>
              <TouchableOpacity onPress={() => this.showAllTalkList()}>
                <Button mode="text" uppercase={false} color="#2DA3FC">All talk</Button>
                </TouchableOpacity>
              </View>}

          {!this.state.isTalkSelectd && 
            <View style={styles.buttonContainer}>
              <Button mode="text" uppercase={false} color="#2DA3FC" labelStyle={{fontWeight:'bold'}} >Reminder</Button>
              <Badge style={{backgroundColor:'#C4E4FD',alignSelf:'center',color:"#2DA3FC",fontWeight:'bold'}} >{this.state.reminderList.length}</Badge>
            </View>
          }
        </View>       




    
        <View style={{flex: 1,position:'relative',width:'100%'}}>

         
        {this.state.isTalkSelectd && 
              <FlatList
                  data={this.state.talkList}
                  renderItem={renderItemAllTalk}
                  keyExtractor={(item) => item.talk_date}
                  ItemSeparatorComponent = { FlatListItemSeparator }
                  onEndReachedThreshold={0.5}
                  onEndReached={this.loadMoreDataOfTalkList}
                />
        }
          
        

        {!this.state.isTalkSelectd && 
            <FlatList
                  data={this.state.reminderList}
                  renderItem={renderItemReminder}
                  keyExtractor={(item) => item.remaind_date}
                  ItemSeparatorComponent = { FlatListItemSeparator }
                  onEndReachedThreshold={0.5}
                  onEndReached={this.loadMoreDataOfReminderList}

                />

              }
         
        </View>

       {!this.state.spinner && <FAB
          style={styles.fab}
          small
          icon="plus"
          color="white"
          onPress={() => this.openAddTalkScreen()}
        />}

      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  fab: {
    position: 'absolute',
    margin: 30,
    right: 0,
    bottom: 0,
    backgroundColor:"#2DA3FC"
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    borderBottomWidth :2,
    borderBottomColor: '#57a3f5',
    marginLeft:15,
    marginRight:15
  },
  buttonContainer2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    marginLeft:15,
    marginRight:15,
    opacity:.4
  },
  spinnerTextStyle: {
    color: '#000',
  },
  rightTopText: {
    color: '#2DA3FC',
    fontWeight: 'bold',
    fontSize: 14,
    includeFontPadding: false,
    marginLeft:5,
    lineHeight: 20,
  },
  rightBottomText: {
    color: '#2DA3FC',
    fontSize: 14,
    includeFontPadding: false,
    paddingBottom: 4,
    paddingLeft: 4,
    lineHeight: 20,
  }
});

export default TalkScreen;
