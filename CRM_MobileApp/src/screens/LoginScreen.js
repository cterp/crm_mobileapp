import React, {useState} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { baseUrl } from '../components/Global';
import {Text} from 'react-native-paper';
import Background from '../components/Background1';
import Logo from '../components/Logo';
import ME_Icon from '../assets/ME_Icon';

import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';
import {theme} from '../core/theme';
import {emailValidator} from '../helpers/emailValidator';
import {passwordValidator} from '../helpers/passwordValidator';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';


const LoginScreen = ({route,navigation}) => {
  const [email, setEmail] = useState({
    value: 'smsadmin1@gmail.com',
    error: '',
  });
  const [password, setPassword] = useState({value: 'xYesCkps5s', error: ''});
  const [loading, setLoading] = useState(false);
  //const [animating] = { true }
  let animating = false;
  let isLoading = false;

  const saveData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value);
      startLoading(false);
      navigation.reset({
        index: 0,
        routes: [{name: 'Dashboard'}],
      });
    } catch (e) {
      startLoading(false);
      alert('Failed to save the data to the storage:' + e.message);
    }
  };

  const startLoading = (isShow) => {
    setLoading(isShow);
  };

  const onLoginPressed = () => {


    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError) {
      isLoading = false;
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }
    startLoading(true);
    let data = {
      username: email.value,
      password: password.value,
      device_token: '1234567890',
    };
    const formData = new FormData();
    formData.append('data', JSON.stringify(data));
    fetch('http://dalsaniya.com/'+baseUrl[0]+'/app_api/auth', {
      method: 'post', //Request Type
      body: formData,
    })
      .then((response) => response.json())
      //If response is in json then in success
      .then((responseJson) => {
        
        isLoading = false;
        if (responseJson.statusCode == 401) {
          alert(responseJson.message);
          startLoading(false);
        } else if (responseJson.statusCode == 200) {
          saveData('@LoginData', JSON.stringify(responseJson));
        }
        console.log(responseJson);
      })
      //If response is not in json then in error
      .catch((error) => {
        startLoading(false);
        alert(JSON.stringify(error));
        console.error(error);
      });
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={true}
      automaticallyAdjustContentInsets={false}
      keyboardDismissMode="on-drag"
      style={{backgroundColor: 'pink'}}
      contentContainerStyle={{flexGrow: 1}}
      keyboardShouldPersistTaps="handled">
      <Background>
        <Spinner
          //visibility of Overlay Loading Spinner
          visible={loading}
          color="#085cab"
        />
        <ME_Icon />


        <View
          style={{backgroundColor: 'white', width: '100%',marginTop:50, borderRadius: 20}}>
          <View style={{height: '10%'}}>
            <Text style={styles.label}>Login</Text>
          </View>
          <View style={{marginTop: 0}}>
            <TextInput
              label="Email"
              returnKeyType="next"
              value={email.value}
              onChangeText={(text) => setEmail({value: text, error: ''})}
              error={!!email.error}
              errorText={email.error}
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
            />
            <TextInput
              label="Password"
              returnKeyType="done"
              value={password.value}
              onChangeText={(text) => setPassword({value: text, error: ''})}
              error={!!password.error}
              errorText={password.error}
              secureTextEntry
            />
          </View>
          <View style={styles.forgotPassword}>
            <TouchableOpacity
              onPress={() => navigation.navigate('ForgotPasswordScreen')}>
              <Text style={styles.forgot}>Forgot password?</Text>
            </TouchableOpacity>
          </View>

          <Button mode="contained" onPress={onLoginPressed}>
            Login
          </Button>
        </View>
        
      </Background>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#000',
  },
  forgotPassword: {
    width: '95%',
    alignItems: 'flex-end',
    marginBottom: 24,
    color: 'white',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 15,
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  label: {
    color: '#57a3f5',
    margin: 0,
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default LoginScreen;
