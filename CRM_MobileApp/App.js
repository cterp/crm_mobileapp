import React from 'react'
import { Provider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import { theme } from './src/core/theme'
import {
  ReminderList,
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  Dashboard,
  CustomerDetail,
  CustomerList,
  AddCustomerScreen,
  FilterScreen,
  AddTalkScreen,
  TalkScreen,
  EmployeeList,
  SCFilterScreen,
  CompanyNameScreen,
  SplashScreen,
} from './src/screens'

const Stack = createStackNavigator()

const App = () => {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="ReminderList" component={ReminderList} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="CustomerDetail" component={CustomerDetail} />
          <Stack.Screen name="CustomerList" component={CustomerList} />
          <Stack.Screen
            name="ForgotPasswordScreen"
            component={ForgotPasswordScreen}
          />
          <Stack.Screen name="AddCustomerScreen" component={AddCustomerScreen} />
          <Stack.Screen name="FilterScreen" component={FilterScreen} />
          <Stack.Screen name="AddTalkScreen" component={AddTalkScreen} />
          <Stack.Screen name="TalkScreen" component={TalkScreen} />
          <Stack.Screen name="EmployeeList" component={EmployeeList} />
          <Stack.Screen name="SCFilterScreen" component={SCFilterScreen} />
          <Stack.Screen name="CompanyNameScreen" component={CompanyNameScreen} />
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App
